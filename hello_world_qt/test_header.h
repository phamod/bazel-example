#ifndef BUILD_TESTS_QT5_TEST_HEADER_H
#define BUILD_TESTS_QT5_TEST_HEADER_H

// Nonsense to test ability of qt_cc_library to include
// headers which are not processed by Qt's MOC

#define PIE 971

class Test {

private:
    int a = 3;
public:
    Test() {}
    ~Test(){}
    int getTeam(){return a;}
};

#endif // BUILD_TESTS_QT5_TEST_HEADER_H
