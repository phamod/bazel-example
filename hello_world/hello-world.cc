//#include "lib/hello-time.h"
//#include "hello_world/hello-greet.h"
#include <iostream>
#include <string>
#include "lib/json.hpp"

using nlohmann::json;

int main(int argc, char** argv) {
  std::string who = "world";
  if (argc > 1) {
    who = argv[1];
  }
  //std::cout << get_greet(who) << std::endl;
  //print_localtime();
  std::cout << "hello world!" << std::endl;
  #ifdef BAZEL_BUILD
  std::cout << "a bazel build" << std::endl;
  #else
  std::cout << "not a bazel build" << std::endl;
  #endif

  // instead, you could also write (which looks very similar to the JSON above)
json j2 = {
  {"pi", 3.141},
  {"happy", true},
  {"name", "Niels"},
  {"nothing", nullptr},
  {"answer", {
    {"everything", 42}
  }},
  {"list", {1, 0, 2}},
  {"object", {
    {"currency", "USD"},
    {"value", 42.99}
  }}
};

  std::cout << j2["patrick"] << std::endl;
  if(j2["patrick"] == nullptr){
    std::cout << "patrick is null" << std::endl;
  }
  std::cout << j2.dump(4) << std::endl;

  return 0;
}
